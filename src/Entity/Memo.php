<?php

namespace App\Entity;

use App\Repository\MemoRepository;
use Doctrine\ORM\Mapping as ORM;

/** 
 * @ORM\Entity(repositoryClass=MemoRepository::class)
 */
class Memo
{
    /** 
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /** 
     * @ORM\Column(type="string")
     */
    private $contenu;

    /** 
     * @ORM\Column(type="datetime")
     */
    private $expiration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenu()
    {
        return $this->contenu;
    }

    public function setContenu( $contenu): self
    {
        $this->contenue = $contenu;

        return $this;
    }

    public function getExpiration()
    {
        return $this->expiration;
    }

    public function setExpiration( $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }
}