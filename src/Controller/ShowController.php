<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ShowController extends AbstractController {

    #[
        Route(
            path:'/memo',
            name: 'memo'
        )
    ]
    public function show(Request $Request)
    {
        $response = new Response();
        
        // // creates a task object and initializes some data for this example
        // $memo = new $Memo();
        // $memo->setMemo('Ecrire un memo');

        // $form = $this->createFormBuilder($memo)
        //     ->add('task', TextType::class)
        //     ->add('save', SubmitType::class, ['label' => 'Creer Memo'])
        //     ->getForm();
        return $this->render('memo/show.html.twig');

    }
}