<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController {

    #[
        Route(
            path:'/Home',
            name: 'Home'
        )
    ]
    public function index(Request $Request)
    {
        $response = new Response();
        return $this->render('home/index.html.twig');

    }
}